module.exports = {
	root: true,
	parser: 'babel-eslint',
	parserOptions: {
		sourceType: 'module'
	},
	extends: 'airbnb-base',
	// required to lint *.vue files
	plugins: [
		'html'
	],
	// check if imports actually resolve
	'settings': {
		'import/resolver': {
			'webpack': {
				'config': 'build/webpack.base.conf.js'
			}
		}
	},
	// add your custom rules here
	'rules': {
		// don't require .vue extension when importing
		'import/extensions': ['error', 'always', {
			'js': 'never',
			'vue': 'never'
		}],
		'no-tabs': 0,
		'no-plusplus': 0,
		'import/no-unresolved': 0,
		'import/prefer-default-export': 0,
		"no-restricted-syntax": [0, "FunctionExpression", "ForOfStatement"],
		'indent': [2, 'tab'],
		'no-underscore-dangle': ['error', { 'allowAfterThis': true }],
		'padded-blocks': [2, 'always'],
		'func-names': 0,
		'no-console': 0,
		'no-param-reassign': 0,
		'max-len': ["error", 160, 4],
		// allow debugger during development
		'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
	}
}

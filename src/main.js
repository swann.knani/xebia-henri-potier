import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import App from './App';
import store from './vuex/store';
import router from './router';
import { currency } from './filter/currency';

Vue.filter('currency', currency);

sync(store, router);

const app = new Vue({
	el: '#app',
	template: '<App/>',
	router,
	store,
	...App,
});

export { app, router, store };

import api from '../api';

import {
	RECEIVE_PRODUCTS,
	ADD_TO_CART,
	REMOVE_FROM_CART,
	CHECKOUT_REQUEST,
	CHECKOUT_SUCCESS,
} from './mutation-types';

export const getAllProducts = ({ commit }) => {

	api.getProducts((products) => {

		commit(RECEIVE_PRODUCTS, { products });

	});

};

export const addToCart = ({ commit }, product) => {

	commit(ADD_TO_CART, product);

};

export const removeFromCart = ({ commit }, product) => {

	commit(REMOVE_FROM_CART, product);

};

export const checkout = ({ commit, state }) => {

	const savedCartItems = [...state.cart.added];

	commit(CHECKOUT_REQUEST);

	api.getTotal(
		savedCartItems,
		({ total, discount }) => commit(CHECKOUT_SUCCESS, { total, discount }),
	);

};

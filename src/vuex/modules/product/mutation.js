import { RECEIVE_PRODUCTS } from '../../mutation-types';

export default {

	[RECEIVE_PRODUCTS](state, { products }) {

		state.all = products.map((product) => {

			product.quantity = 0;

			return product;

		});

	},

};

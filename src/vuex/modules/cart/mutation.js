import {
	ADD_TO_CART,
	REMOVE_FROM_CART,
	CHECKOUT_REQUEST,
	CHECKOUT_SUCCESS,
} from '../../mutation-types';

export default {

	[ADD_TO_CART](state, product) {

		const record = state.added.find(p => p.isbn === product.isbn);

		if (!record) {

			product.quantity = 1;

			state.added.push(product);

		} else {

			product.quantity++;

		}

	},

	[REMOVE_FROM_CART](state, product) {

		const record = state.added.find(p => p.isbn === product.isbn);

		if (record) {

			product.quantity--;

			if (!record.quantity) {

				const index = state.added.findIndex(p => p.isbn === product.isbn);

				state.added.splice(index, 1);

			}

		}

	},

	[CHECKOUT_REQUEST](state) {

		state.total = 0;
		state.discount = 0;

	},

	[CHECKOUT_SUCCESS](state, { total, discount }) {

		state.total = total;
		state.discount = discount;

	},

};

export const allProducts = state => state.product.all;

export const allProductsAdded = state => state.cart.added;

export const totalProductsAdded = state => state.cart.total;

export const discountProductsAdded = state => state.cart.discount;

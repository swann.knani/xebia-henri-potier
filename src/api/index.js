import request from 'axios';

request.defaults.baseURL = 'http://henri-potier.xebia.fr';

export const decomposeProducts = (products) => {

	const result = [];

	products.map((product) => {

		let { quantity } = product;

		while (quantity--) {

			result.push(product);

		}

		return product;

	});

	return result;

};

/* eslint-disable arrow-body-style */
export const getDiscounts = (products, total, offers) => {

	/* eslint-enable arrow-body-style */

	return offers.map((offer) => {

		if (offer.type === 'percentage') {

			offer.discount = total * (100 - offer.value) * 0.01;

		} else if (offer.type === 'minus') {

			offer.discount = total - offer.value;

		} else if (offer.type === 'slice') {

			offer.discount = total - (Math.floor(total / offer.sliceValue) * offer.value);

		}

		return offer;

	}).sort((a, b) => a.discount > b.discount);

};


export default {

	async getProducts(success) {

		const { data } = await request.get('/books');

		return success(data);

	},

	async getTotal(payload, success) {

		const products = decomposeProducts(payload);

		const isbn = products.map(p => p.isbn).join();

		const { data: { offers } } = await request.get(`/books/${isbn}/commercialOffers`);

		const total = products.map(p => p.price).reduce((a, b) => a + b);

		const discounts = getDiscounts(products, total, offers);

		const discount = Array.isArray(discounts) ? discounts[0].discount : null;

		return success({ total, discount });

	},

};

import Vue from 'vue';
import VueRouter from 'vue-router';
import Products from 'components/Products';
import Checkout from 'components/Checkout';

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	base: __dirname,
	routes: [
		{ path: '/', component: Products },
		{ path: '/checkout', component: Checkout },
	],
});

export default router;
